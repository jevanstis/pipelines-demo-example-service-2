# Example Service

Demo of Pipelines Docker functionality and service containers. 
Just a service that is dockerised and connected to postgres. No business logic.


---------------

## Usage


Build:
sdf
```
$ mvn package
```

Docker usage:

```
$ docker build -t <name>/example-service:local .
$ docker run -p 9001:9001 -d <name>/example-service:local
```

Then query as you need.

